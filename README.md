# ListView, TextField, ListTile, GridView, Nested Listview - Ostad Assignment Module 6

### Creating very common layout for flutter. Here 2 main screen added. First Screen is showing static way of design. and Second Screen is showing DYNAMIC way of design where Listview is main widget for scroll and within this ListView I used TextField for search and GridView for Grid lauout and after this I added a Listview Separated so that we can see a list of widget vertically with a divider line. Any question regarding this email me at dr.has82@gmail.com


#### Screenshot is output design

<br />
<br />

<img src="https://gitlab.com/hasan082/ostad-listview-gridview-within-listview/-/raw/main/screenshot-2.png" alt="Column wrap List tile" width="270" >

<img src="https://gitlab.com/hasan082/ostad-listview-gridview-within-listview/-/raw/main/screenshot-2.png" alt="Grid view list view" width="270" >
