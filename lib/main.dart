import 'package:flutter/material.dart';
import 'package:ostad_assignment_grid_list_view/pages/dynamic_way.dart';

void main() {
  runApp(PhotoGalleryApp());
}

class PhotoGalleryApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: PhotoGalleryScreen(),
    );
  }
}

class PhotoGalleryScreen extends StatelessWidget {

  Widget buildPhotoWidget(BuildContext context, String imageUrl, String caption) {
    return AspectRatio(
      aspectRatio: 1.0,
        child: TextButton(
            onPressed: () {
              showSnackbar(context, 'Clicked on photo!');
            },
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.network(
                  imageUrl,
                  fit: BoxFit.cover,
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 2, horizontal: 7),
                      decoration: BoxDecoration(color: Colors.black45),
                      child: Text(
                        caption,
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      ),
                    )),
              ],
            ),
          ),
    );
  }

  Widget listTilesWidget({String? title, String? category}) {
    return Card(
      margin: EdgeInsets.only(top: 5, bottom: 5, left: 8, right: 8),
        child: ListTile(
          leading: Icon(Icons.photo_album),
          title: Text(
            title!,
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text(category!),
        ),
    );
  }

  void showSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message), duration: Duration(milliseconds: 1000),),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo Gallery'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Welcome to My Photo Gallery!',
                style: TextStyle(fontSize: 24.0),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: TextField(
                autofocus: false,
                decoration: InputDecoration(
                  labelText: 'Search',
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                ),
              ),
            ),
            SizedBox(height: 16.0),
            Wrap(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/400/400', 'Caption 1'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/450/450', 'Caption 2'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/420/420', 'Caption 3'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/350/350', 'Caption 4'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/310/310', 'Caption 5'),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: buildPhotoWidget(
                      context, 'https://picsum.photos/250/250', 'Caption 6'),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Column(
              children: [
                listTilesWidget(
                    title: 'Sample Photo 1', category: 'Category 1'),
                listTilesWidget(
                    title: 'Sample Photo 2', category: 'Category 2'),
                listTilesWidget(
                    title: 'Sample Photo 3', category: 'Category 3'),
                listTilesWidget(
                    title: 'Sample Photo 4', category: 'Category 4'),
                listTilesWidget(
                    title: 'Sample Photo 5', category: 'Category 5'),
                listTilesWidget(
                    title: 'Sample Photo 6', category: 'Category 6'),
                // Add more list items here...
              ],
            ),
            SizedBox(height: 30.0),
            Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: 30),
              width: 100,
              height: 45,
              child: ElevatedButton(
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const DifferentWay(),
                    ),
                  );
                },
                child: Text('Different Approach'),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showSnackbar(context, 'Photos Uploaded Successfully!');
        },
        child: Icon(Icons.cloud_upload),
      ),
    );
  }


}
