import 'package:flutter/material.dart';

class DifferentWay extends StatefulWidget {
  const DifferentWay({Key? key}) : super(key: key);

  @override
  State<DifferentWay> createState() => _DifferentWayState();
}

class _DifferentWayState extends State<DifferentWay> {
  bool isFocused = false; // For TextField icon color change during focus

  final List<String> itemTitles = [
    'Caption',
    'Caption',
    'Caption',
    'Caption',
    'Caption',
    'Caption',
  ];
  final List<String> photoTitles = [
    'Simple Photo',
    'Simple Photo',
    'Simple Photo',
    'Simple Photo',
    'Simple Photo',
    'Simple Photo',
  ];

  void showSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          content: Text(message),
          duration: Duration(milliseconds: 1000),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            "Gallery(List & Grid)",
            style: TextStyle(fontSize: 20),
          ),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showSnackbar(context, 'Photos Uploaded Successfully!');
          },
          child: Icon(Icons.cloud_upload),
        ),
        body: Padding(
          padding:
              const EdgeInsets.only(top: 20.0, left: 15, right: 15, bottom: 20),
          child: ListView(
            children: [
              const Text(
                'Welcome to My Photo Gallery',
                style: TextStyle(fontSize: 23.0),
              ),
              const SizedBox(
                height: 20,
              ),
              Focus(
                onFocusChange: (hasFocus) {
                  setState(() {
                    isFocused = hasFocus;
                  });
                },
                child: TextField(
                  autofocus: false,
                  decoration: InputDecoration(
                      labelText: 'Search',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey.shade400),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      prefixIcon: Icon(
                        Icons.search,
                        color: isFocused ? Colors.blue : Colors.grey,
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10)),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 7,
                  mainAxisSpacing: 7,
                ),
                itemCount: itemTitles.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: (){
                      showSnackbar(context,'Clocked On Photo ${index + 1}');
                    },
                    child: Stack(
                      children: [
                        AspectRatio(
                          aspectRatio: 1,
                          child: Image(
                            image: NetworkImage(
                                'https://picsum.photos/4${index}0/4${index}0'),
                            fit: BoxFit.cover,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 7),
                            color: Colors.black54,
                            child: Text(
                              '${itemTitles[index]} ${index + 1}',
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 15),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              ListView.separated(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemCount: photoTitles.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 2,
                    child: ListTile(
                      leading: const Icon(Icons.photo_album),
                      title: Text('${photoTitles[index]} ${index + 1}'),
                      subtitle: Text('Category $index'),
                      onTap: (){
                        showSnackbar(context,'Clocked On List No ${index + 1}');
                      },
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(height: 5);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
